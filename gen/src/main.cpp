#include "landscape_generator.h"
#include "midpoint_displacement.h"
#include <iostream>

int main()
{
    gen::LandscapeGenerator landscape_generator { };
    gen::HeightMap height_map = landscape_generator
        .add_mutation(gen::MidpointDisplacement(100))
        .build_height_map(5, 5);

    gen::print_map(std::cout, height_map);
    system("pause");
}
