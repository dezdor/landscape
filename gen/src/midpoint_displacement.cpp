#include "midpoint_displacement.h"

namespace gen 
{
    MidpointDisplacement::MidpointDisplacement(int detalization) : m_detalization(detalization)
    {

    }

    void MidpointDisplacement::operator() (HeightMap& height_map)
    {
        // getting block size
        auto block_size = get_block_size(m_detalization);

        if (block_size > std::min(height_map.width(), height_map.length()))
        {
            block_size = get_max_block_size(height_map);
        }

        // main algorithm loop
        while (block_size > 1)
        {
            diamond_step(height_map, block_size);
            square_step(height_map, block_size);

            block_size /= 2;
        }

        /* to be finished */
    }

    void MidpointDisplacement::diamond_step(HeightMap& height_map, int block_size)
    {
        auto half_block = block_size / 2;

        auto generator = std::default_random_engine();
        auto distribution = std::uniform_real_distribution<HeightMap::value_type>(-half_block, half_block);
        auto randomizer = std::bind(distribution, generator);

        for (auto current_x = 0; current_x < height_map.width() - 1 ; current_x += block_size)
        {
            for (auto current_y = 0; current_y < height_map.length() - 1; current_y += block_size)
            {
                // get the square corner values (names are simple because of little scope of their life)
                auto a = height_map.mirror_at(current_x + block_size, current_y + block_size);
                auto b = height_map.mirror_at(current_x, current_y + block_size);
                auto c = height_map.mirror_at(current_x + block_size, current_y);
                auto d = height_map.mirror_at(current_x, current_y);

                // add all the corner values
                auto midpoint_value = (a + b + c + d) / 4;

                // set the square center value
                height_map.mirror_at(current_x + half_block, current_y + half_block) = midpoint_value + randomizer();
            }
        }
    }

    void MidpointDisplacement::square_step(HeightMap& height_map, int block_size)
    {
        auto half_block = block_size / 2;
        auto quarter_block = half_block / 2;

        auto generator = std::default_random_engine();
        auto distribution = std::uniform_real_distribution<HeightMap::value_type>(-quarter_block, quarter_block);
        auto randomizer = std::bind(distribution, generator);

        for (auto current_x = 0; current_x < height_map.width() - 1; current_x += block_size)
        {
            for (auto current_y = 0; current_y < height_map.length() - 1; current_y += block_size)
            {
                // get the diamonds corner values (names are simple because of little scope of their life)
                auto a = height_map.mirror_at(current_x + half_block, current_y - half_block);
                auto b = height_map.mirror_at(current_x - half_block, current_y + half_block);
                auto c = height_map.mirror_at(current_x + half_block, current_y + half_block);
                auto d = height_map.mirror_at(current_x + block_size, current_y);
                auto e = height_map.mirror_at(current_x, current_y + block_size);
                auto f = height_map.mirror_at(current_x, current_y);

                // set the half-edge values
                auto midpoint_value = (a + c + d + f) / 4;
                height_map.mirror_at(current_x + half_block, current_y) = midpoint_value + randomizer();

                midpoint_value = (b + c + e + f) / 4;
                height_map.mirror_at(current_x, current_y + half_block) = midpoint_value + randomizer();
            }
        }
    }

    int get_block_size(int detalization)
    {
        return static_cast<int>(std::pow(2, detalization));
    }

    int get_max_block_size(const HeightMap& height_map)
    {
        int max_detalization = 0;
        auto min_side = std::min(height_map.width(), height_map.length());

        while (min_side >>= 1) ++max_detalization;

        return get_block_size(max_detalization);
    }
}

