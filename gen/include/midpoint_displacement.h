#ifndef H_MUTATION_ALGORITHMS_INCLUDED
#define H_MUTATION_ALGORITHMS_INCLUDED

#include <functional>
#include <algorithm>
#include <random>
#include "height_map.h"

namespace gen
{
    int get_block_size(int detalization);
    int get_max_block_size(const HeightMap& height_map);

    class MidpointDisplacement 
    {
    public:
        MidpointDisplacement(int detalization = 0);
        void operator() (HeightMap& height_map);

    private:
        void diamond_step(HeightMap& height_map, int block_size);
        void square_step(HeightMap& height_map, int block_size);

    private:
        int m_detalization;
    };
}

#endif//H_MUTATION_ALGORITHMS_INCLUDED